# SMS sender

Send a message in bulk to a given list of recipients. Written in Python 3 and utilizes Twilio's services.

## Installing
Before you can start sending SMS messages, you'll need a paid Twilio account. No purchased Twilio number is required for this script.

This project requires [Twilio's Python helper library](https://www.twilio.com/docs/libraries/python).
```shell
pip3 install twilio
```

## Usage
Your message will be stripped of newlines. Phone numbers should formatted as one number per line and prefixed with country code, for example `+31612345678`. Currently each SMS sent using this method costs 9 dollar cents per recipient per segment of 140 characters.
```shell
python3 send.py --from-name "From Name" \
  --message message.txt \
  --recipients recipients.txt \
  --twilio-account your_twilio_account_sid \
  --twilio-token your_twilio_account_token
```

Example usage:
```shell
$ python3 send.py -f "Albert Einstein" -m message.txt -r recipients.txt -a [...] -t [...]
The following message will be sent to 2 recipients:
From: Albert Einstein
The lazy fox jumps over the quick brown fox.

(characters: 44, segments: 1)
Send message to 2 recipients? [y/N] y
Sent message! (SID: SM...)
Sent message! (SID: SM...)
Sent 2 messages (100% success rate).
$
```

