#!/usr/bin/python3
import argparse

parser = argparse.ArgumentParser(description='Send a message to given phonenumbers using twilio.')
parser.add_argument('--from-name', '-f', help='name recipients will see with sms', required=True)
parser.add_argument('--message', '-m', help='path to a file containing a message', required=True)
parser.add_argument('--recipients', '-r', help='path to a file containing phonenumbers', required=True)
parser.add_argument('--twilio-account', '-a', help='twilio account sid', required=True)
parser.add_argument('--twilio-token', '-t', help='twilio authentication token', required=True)
args = parser.parse_args()

try:
    from twilio.rest import Client
except ModuleNotFoundError as e:
    print("Please install the twilio Python 3 module using pip.")

def load_file(path):
    with open(path, 'r') as f:
        return f.readlines()

def send_sms(client, recipient, from_name, message):
    try:
        message = client.messages.create(
            body=message,
            from_=from_name,
            to=recipient
        )
        print(f"Sent message! (SID: {message.sid}")
        return True
    except Exception as e:
        print(f"Sending message to {recipient} failed: {str(e)}")
        return False

def main():
    message = "\n".join(load_file(args.message)).strip()
    from_name = args.from_name
    recipients = load_file(args.recipients)
    num_segments = round(len(message) / 140)
    client = Client(args.twilio_account, args.twilio_token)
    success = 0

    print(f"The following message will be sent to {str(len(recipients))} recipients:")
    print(f"From: {from_name}\n{message}\n\n(characters: {str(len(message))}, segments: {str(num_segments)})")
    if input(f"Send message to {str(len(recipients))} recipients? [y/N] ").lower() != 'y':
        return False

    for recipient in recipients:
        recipient = recipient.strip()
        if send_sms(client, recipient, from_name, message):
            success += 1

    success_perc = round( (success / len(recipients)) * 100)
    print(f"Sent {str(success)} messages ({str(success_perc)}% success rate).")

main()
